package comptebancaire;

import static org.junit.Assert.assertEquals;


import org.junit.jupiter.api.Test;

public class CompteTest {

	@Test
	public void testCompte() {
		Compte c = new Compte();
		assertEquals(0,c.consultation());
	}

	@Test
	public void testCompteInt() {
		Compte c = null;
		try {
			c = new Compte(100);
		}
		catch (ExceptionSommeNegative e){}
			assertEquals(100,c.consultation());
	}

	@Test
	public void testConsultation() {
		Compte c = null;
		try {
			c = new Compte(100);
		}
		catch (ExceptionSommeNegative e){}
			assertEquals(100,c.consultation());
	}

	@Test
	public void testCrediter() {
		Compte c = null;
		try {
			c = new Compte(100);
		}
		catch (ExceptionSommeNegative e){}
			try {
				c.crediter(10);
			}
			catch (ExceptionSommeNegative e){}
			assertEquals(110,c.consultation());
	}

	@Test
	public void testDebiter() {
		Compte c = null;
		try {
			c = new Compte(100);
		}
		catch (ExceptionSommeNegative e){}
			try {
					c.debiter(10);
			}
			catch (ExceptionCompteAdecouvert e) {	
			}
			assertEquals(90,c.consultation());
	}

	@Test
	public void testVirement() {
		Compte c = null;
		Compte c1 = null;
		try {
			c = new Compte(100);
			c1 = new Compte(1000);
		}
		catch (ExceptionSommeNegative e){}

		try {
			c.virement(c1, 50);
		} catch (ExceptionSommeNegative | ExceptionCompteAdecouvert e) {

		}
		assertEquals(1050,c1.consultation());
	}

}
