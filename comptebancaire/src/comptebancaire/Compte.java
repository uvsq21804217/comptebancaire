package comptebancaire;

public class Compte {
	private int solde;

	public Compte(){
		this.solde = 0;
	}
	
	public Compte(int solde)throws ExceptionSommeNegative {
		if (solde < 0) throw new ExceptionSommeNegative(); 
		this.solde = solde;
	}
	
	public int consultation() {
		return this.solde;
	}
	
	public void crediter(int credit) throws ExceptionSommeNegative {
		if (solde < 0) throw new ExceptionSommeNegative();
		this.solde += credit;
	}
	
	public void debiter(int debit) throws ExceptionCompteAdecouvert {
		if ((this.solde - debit) < 0) throw new ExceptionCompteAdecouvert();
		else 
		this.solde -= debit;
	}
	
	public void virement(Compte compte2, int credit)throws ExceptionSommeNegative,ExceptionCompteAdecouvert  {
			if (credit < 0) throw new ExceptionSommeNegative();
			if ((compte2.consultation() - credit )< 0) throw new ExceptionCompteAdecouvert();
			compte2.crediter(credit);
	}
}